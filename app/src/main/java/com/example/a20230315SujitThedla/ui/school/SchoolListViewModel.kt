package com.example.a20230315SujitThedla.ui.school

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import com.example.a20230315SujitThedla.NYCApplication
import com.example.a20230315SujitThedla.models.SchoolDetails
import com.example.a20230315SujitThedla.repositories.AppRepository
import javax.inject.Inject

class SchoolListViewModel : ViewModel() {
    @JvmField
    @Inject
    var appRepository: AppRepository? = null

    init {
        NYCApplication.application?.appComponent?.inject(this)
    }

    val schoolDetailsList: LiveData<PagedList<SchoolDetails?>>?
        get() = appRepository?.schoolDetailsList
}