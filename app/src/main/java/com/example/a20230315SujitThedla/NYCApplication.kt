package com.example.a20230315SujitThedla

import android.app.Application
import com.example.a20230315SujitThedla.di.AppComponent
import com.example.a20230315SujitThedla.di.AppModule
import com.example.a20230315SujitThedla.di.DaggerAppComponent

class NYCApplication : Application() {
    // Instance for Dagger 2 Component:
    lateinit var appComponent: AppComponent
        private set

    override fun onCreate() {
        super.onCreate()
        application = this

        // Instantiate instance for Dagger 2 Component
        appComponent = DaggerAppComponent.builder().appModule(AppModule()).build()
        appComponent.inject(this)
    }

    companion object {
        @JvmStatic
        var application: NYCApplication? = null
            private set
    }
}